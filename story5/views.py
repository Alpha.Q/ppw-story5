from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm
from django.http import HttpResponse


# def index(request):
#     return HttpResponse("Hello, world")

# def news(request):
#     return render(request,"profile_page/news.html")

def index(request):
    schedule_list = Schedule.objects.all()
    return render(request, 'schedule/index.html', {'schedules': schedule_list})

def add(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            name = request.POST['name']
            date = request.POST['date']
            place = request.POST['place']
            category = request.POST['category']
            message = request.POST['message']

            schedule = Schedule(name = name, date = date, place = place, category = category, message = message)
            schedule.save()
    return redirect('/')

def remove(request):
    if request.method == 'POST':
        schedule_id = request.POST['id']
        schedule = Schedule.objects.get(id = int(schedule_id))
        schedule.delete()
    return redirect('/')
