from django import forms

class ScheduleForm(forms.Form):
    CATEGORY_TYPE_CHOICES = [('College', 'College'), ('Competition', 'Competition'), ('Others', 'Others')]
    
    name = forms.CharField(max_length=255, required = True)
    date = forms.DateTimeField(input_formats=['%Y-%m-%dT%H:%M'])
    place = forms.CharField(max_length=255, required = False)
    category = forms.ChoiceField(choices=CATEGORY_TYPE_CHOICES, required=True)
    message = forms.CharField(max_length=255, required=False)
