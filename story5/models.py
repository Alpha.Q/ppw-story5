from django.db import models

# Create your models here.

class Schedule(models.Model):
    CATEGORY_TYPE_CHOICES = [('College', 'College'), ('Competition', 'Competition'), ('Others', 'Others')]

    name = models.CharField(max_length=255, default='')
    date = models.DateTimeField('')
    place = models.CharField(max_length=255, default='')
    category = models.CharField(max_length=255, choices=CATEGORY_TYPE_CHOICES, default='College')
    message = models.CharField(max_length=255, default='')
